# Abstract Formats #

##  ESMRMB ##

### Scientific Session communications ###

Abstracts must be limited to **400 words** and structured as follows:

* Purpose/Introduction
* Subjects and Methods
* Results
* Discussion/Conclusion
* References

### Clinical Review Poster Session communications ###

Abstracts must be limited to **400 words** and structured as follows:

* Introduction
* Cases
* Discussion
* References

### Software Exhibit communications ###

Abstracts must be limited to **400 words** and structured as follows:

* Purpose of the software
* Methods/Implementation/Hardware requirements
* Features illustrated at the Exhibit

## RSNA ##

### Scientific presentation ###

Abstracts are limited to **2200 characters including spaces**, and are to be constructed using the following section headings: 

* Purpose
* Materials and Methods
* Results 
* Conclusion

*A Clinical Relevance statement, not to exceed 200 characters, is also required.*

### Applied science ###

Abstracts are limited to **2200 characters** and are to be constructed using the following section headings:

* Background 
* Evaluation 
* Discussion 
* Conclusion
